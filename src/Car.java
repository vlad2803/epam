
/**
 * Created by Ковалев on 26.05.2017.
 */
public class Car {
    private String brand;
    private String model;
    private int age;
    private Engine engine;
    private Transmission transmission;


    public Car(String brand, String model, int age, Engine engine, Transmission transmission) {
        this.brand = brand;
        this.model = model;
        this.age = age;
        this.engine = engine;
        this.transmission=transmission;


    }
    public void startEngine() {
        if (!engine.isStarted()) {
            engine.start();
        } else {
            System.out.println("You can't start engine twice!");
        }
    }

        public void accelerate() {
            if (engine.isStarted()) {
                engine.increaseRam();
                System.out.println("Speed increased: " + (engine.getCurrentRam()));
            } else {
                System.out.println("You should start engine before accelerate!");
            }
        }

    public void Upper() {
        if (engine.isStarted()) {
            transmission.UpShift();
            System.out.println("Speed increased: " + (transmission.getCurrentRam())+"Shift"+transmission.getShift());
        } else {
            System.out.println("You should start engine before accelerate!");
        }
    }

    public void downSh() {
        if (engine.isStarted()) {
            transmission.DownShift();
            System.out.println("Speed increased: " + (transmission.getCurrentRam())+"Shift"+transmission.getShift());
        } else {
            System.out.println("You should start engine before accelerate!");
        }
    }


    public void stopEngine() {
        if (engine.isStarted()) {
            engine.stop();
        } else {
            System.out.println("Engine already stopped!");
        }
    }

}
