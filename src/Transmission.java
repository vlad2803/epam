/**
 * Created by Ковалев on 26.05.2017.
 */
public class Transmission {
    private String typeTransmission;
    private int shift;
    private int ram;
    private int ThisShift=0;



    public Transmission(String typeTransmission, int shift)
    {
        this.typeTransmission=typeTransmission;
        this.shift=shift;


    }

    public void UpShift() {
        if (ThisShift<shift) {
            ram = 850;
            ThisShift++;
            System.out.println("shift: " + ThisShift);
        }
        else System.out.println("this last shift");
    }

    public void DownShift() {
        if (ThisShift>1) {
            ram = ram+2000;
            ThisShift--;
            System.out.println("shift: " + ThisShift);
        }
        else System.out.println("this first shift");
    }

    public int getCurrentRam() {
        return ram;
    }
    public int getShift() {
        return ThisShift;
    }
}
