/**
 * Created by Ковалев on 26.05.2017.
 */
public class Engine  {
    private int power;

    private String typeEngine;
    private boolean started;
    private int ram;

    public Engine(boolean started, int power, String typeEngine) {
        this.power  = power;
        this.typeEngine=typeEngine;
        started = false;
    }

    public boolean isStarted() {
        return started;
    }

    public void start() {
        if (typeEngine=="B")
                ram=890;
                else ram=800;
        System.out.println("Engine started! ram: "+ram);
        started = true;
    }
    public void stop() {
        System.out.println("Engine stopped");
        ram=0;
        started = false;
    }
    public int getCurrentRam() {
        return ram;
    }

    public void increaseRam() {
        ram=ram+50;
    }

}
